import { Link } from "react-router-dom";
import {
  Linkedin,
  Gitlab,
  Stackoverflow,
  Github,
  Facebook,
  Minutemailer,
  Duolingo
} from "@icons-pack/react-simple-icons";

const expYears = () => {
  let prev_jobs_sec = (1 + 1 + 5) * 30 * 86400; // Patronka, Maria Bohm and Alingsåskonventet
  let start_tf = Date.parse("2014-02-25");
  let diff = (Date.now() - start_tf) / 1000;
  let total = diff + prev_jobs_sec;
  return Number.parseFloat(total / 86400 / 365).toFixed(1);
};

const skills = {
  daily: [
    {
      text: "Angular magic",
      fraction: 0.46,
      tooltip:
        "A daily occurrence at my last Zington client, whose skills & planning platform is build in Node and Angular.",
      tooltipPlacement: "top"
    },
    {
      text: "Misc JS", // better title here?
      fraction: 0.31,
      tooltip: "Node, React, vanilla, a teensy bit of Vue"
    },
    {
      text: "CSS3 x-browser problem solving",
      fraction: 0.15,
      tooltip:
        'I enjoy discussing the subtleties of z-index and hacking up weird stuff in pure CSS, and have at times been the go-to person at work for questions like "but why did the padding-top go crazy when I changed the dimensions of the parent?"'
    },
    {
      text: "Java hacking",
      fraction: 0.1,
      tooltipPlacement: "top",
      tooltip:
        "It's what I was hired for at Kinnarps, and although my work there ended up extremely varied in terms of programming languages, it remained a solid staple."
    },
    {
      text: "Customer service",
      fraction: 0.06,
      tooltipPlacement: "top",
      tooltip:
        "I do a fair amount of customer service (and similar type work), both in the form of spec discussions and tech support."
    },
    {
      text: "Regular expressions",
      fraction: 0.02,
      tooltipPlacement: "top",
      tooltip: (
        <>
          <img
            src="images/xkcd-regex.png"
            alt="XKCD - regular expressions"
            style={{ maxWidth: "20em", width: "100%", background: "white" }}
          />
          <br />
          <em
            style={{
              fontSize: ".8em",
              lineHeight: "1",
              verticalAlign: "top"
            }}
          >
            (xkcd.com)
          </em>
        </>
      )
    }
  ],
  skills: [
    {
      text: "PHP",
      size: 86,
      tooltip:
        "PHP is my 'good old friend' of programming. I first learned it well over 15 years ago and it is still my default go-to among backend languages."
    },
    {
      text: "React.js",
      size: 85,
      tooltip: "See eg: this website 🙃"
    },
    {
      text: "HTML/CSS",
      size: 100,
      tooltip:
        "This is pretty much what I breathe. I started writing HTML when I was twelve and CSS soon thereafter, and quickly saw the value in following web standards while aiming for backwards compatibility."
    },
    {
      text: "Javascript",
      size: 72,
      tooltip:
        "I actually started off with jQuery, but eventually realised that you get much more versatility with vanilla. Go figure! :)"
    },
    {
      text: "Git",
      size: 62,
      tooltip:
        "Who doesn't use either GitLab or GitHub professionally at this point?"
    },
    {
      text: "WordPress",
      size: 56,
      tooltip:
        "My work at The Farm (2014-2018) was easily 90% custom WordPress builds - some straight-forward themeing jobs, others deep-dives into hooks and filters."
    },
    {
      text: "Drupal",
      size: 20,
      tooltip:
        "I've done a few projects in Drupal in the past and use it occasionally in client projects."
    },
    {
      text: "SQL",
      size: 60,
      tooltip: ""
    },
    {
      text: "Vue",
      size: 12,
      tooltip:
        "Did a 'dummy's guide to Vue' type thing recently and found it a really straight-forward and friendly framework."
    },
    {
      text: "Node.js",
      size: 39,
      tooltip:
        "I'm fluent enough to find my way around with the docs in the next tab over."
    },
    {
      text: "Figma",
      size: 22,
      tooltip: ""
    },
    {
      text: "Photoshop",
      size: 77,
      tooltip:
        "Most of the graphical work I do is in Adobe CS or open-source alternatives. I'm comfortable in Photoshop, Illustrator and InDesign."
    }
  ],
  interests: [
    {
      size: 95,
      text: "Usability",
      tooltip:
        "Everything is meant to be used or interacted with, whether it's a text, a T-shirt or a web app. I love things that work frictionlessly."
    },
    {
      size: 30,
      text: "Maths",
      tooltip:
        "I have a Bachelor's degree in Pure Mathematics. The mindset I have when reading proofs is great for finding edge-cases and squashing bugs."
    },
    {
      size: 90,
      text: "Languages",
      tooltip:
        "I speak three, understand the gist of a few more and tend to geek out over phonetics articles on Wikipedia."
    },
    {
      size: 60,
      text: "Steampunk",
      tooltip: "My hat collection is amazing."
    },
    {
      size: 30,
      text: "RPG",
      tooltip:
        "I play bards and characters high in Int. Strongly prefer WoD politics and Ars Magica over punch-monsters-for-XP systems."
    },
    {
      size: 80,
      text: "Designing things",
      tooltip:
        "Things = web sites, ball gowns, PC chassis, corsets, paintings, business cards, furniture, book covers, instruction manuals."
    },
    {
      size: 45,
      text: "Browser quirks",
      tooltip: "I spend half my days on caniuse.com"
    }
  ],
  numbers: [
    {
      classes: "text-left", // do we need it or automatic for number-first??
      content: {
        digit: expYears(),
        text: "years’ professional<br>experience"
      },
      style: "number-first",
      tooltip:
        "Including employment at The Farm Interactive, Kinnarps Sweden and Zington, as well as freelance projects for Maria Bohm, Patronka Sp. z o.o and others."
    },
    {
      classes: "show-four",
      content: {
        digit: "1<sup>st</sup>",
        text: ["BSc<br> with", "class<br> Honours"]
      },
      style: "number-middle",
      tooltip:
        "BSc (Hons) in Mathematics from the University of Aberdeen, UK, followed by a Post-Graduate Certificate in Information Technology and a term at Cambridge."
    },
    {
      classes: "show-five",
      content: {
        digit: "3",
        text: "languages<br> spoken"
      },
      style: "number-first",
      tooltip: "That's English, French and Swedish."
    },
    {
      content: {
        digit: "30+",
        text: '<div style="margin-left:-.2ex">WP sites for major<br> business customers</div>'
      },
      style: "number-first",
      tooltip:
        "I've worked on projects for Helly Hansen, ViFast, HgCapital, Axiell Media and the Astrid Lindgren Company, among others."
    },
    {
      classes: "text-right", // see above, do we need?
      content: {
        digit: "1999",
        text: "Designed for<br> web since"
      },
      style: "number-last",
      tooltip: "Hopefully I've improved some since then ..."
    }
  ]
};

const portfolio = {
  projects: [
    {
      id: "jalle",
      content: {
        title: "Jallerian",
        text: `<p>A new web site to market the Kinnarps AB company restaurant, Jallerian, to the broader public. 
        I indepen&shy;dently wireframed, designed and built the site, in close collaboration with the restaurant 
        manager and head chef to make sure it suited the restaurant's needs, and worked with the 
        marketing department to ensure a fit with the Kinnarps brand identity.</p>
        <p>I used <a href="https://getgrav.org" title="Grav CMS">Grav CMS</a>, with some customisations, 
        as a light-weight admin backend and built a simple REST API for lunch menus, together with a 
        Java portlet that used said API to display menus in the Kinnarps intranet.</p>`
      },
      urls: {
        live: "https://jallerian.se"
      },
      icons: [
        {
          icon: "screen",
          text: "Wireframed & designed from scratch"
        },
        {
          icon: "mobile",
          text: "Mobile-friendly"
        },
        {
          icon: "code",
          text: "Grav CMS admin panel,\nREST API + Java portlet integration"
        }
      ],
      bg: {
        image: "jalle-lg.jpg",
        position: ""
      },
      screens: {
        desktop: "jalle-desktop2.jpg",
        tablet: "jalle-tablet.jpg",
        mobile: "jalle-mobile1.jpg"
      }
    },
    {
      id: "bal",
      content: {
        title: "Steampunkbalen 2017",
        Jsx: () => (
          <>
            <p>
              The Steampunk Ball was a yearly event hosted at a beautiful old
              hall by members of{" "}
              <Link to="../spgbg" relative="path">
                Steampunk Götheborg
              </Link>
              . It was decided that the 2017 instalment needed a new site which
              presented the most important information up front, and which used
              photos from previous balls to set the atmosphere and provide
              inspiration to new guests. My contribution to the project was a
              fully responsive design, HTML mockup and style/design guide.
            </p>
            <p>
              The site was quite heavily optimised for speed and featured modern
              CSS techniques aplenty. This project had me really practice my
              skills at progressive enhancement, as it needed graceful fallbacks
              for IE8, and preferably even older beasts!
            </p>
          </>
        )
      },
      urls: {
        //live: "http://bal.steampunkgbg.se"
      },
      icons: [
        {
          icon: "screen",
          text: "Web design & mockup"
        },
        {
          icon: "mobile"
        },
        {
          icon: "print",
          text: "Posters matching site"
        },
        {
          icon: "non-profit"
        }
      ],
      bg: {
        image: "bal-lg.jpg"
      },
      screens: {
        desktop: "bal-desktop.jpg",
        tablet: "bal-tablet-poster.jpg",
        mobile: "bal-mb.jpg"
      }
    },
    {
      id: "spgbg",
      content: {
        title: "Steampunk Götheborg",
        Jsx: () => (
          <>
            <p>
              When Gothenburg's first steampunk society (and youngest SF club)
              was founded, my&nbsp;involvement in the Alingsås{" "}
              <Link to="../spkon" relative="path">
                Steampunk Convention
              </Link>{" "}
              made it natural that I be in charge of the society's graphics and
              web&nbsp;presence.
            </p>
            <p>
              The site was responsive and featured a customised WordPress
              backend, member-only areas and a simple Google calendar
              integration.
            </p>
            <p>
              On the print side of things, I made two-tone contact cards, flyers
              and posters for events and&mdash;something of a first&mdash;a
              vector design cut into a big piece of brass, which was made into a
              parade sign.
            </p>
            <img
              alt="Contact cards"
              src="/images/portfolio/spgbg-cards.png"
              className="mobile-hide absolute bottom-3.5 left-9 w-1/4 h-auto"
              style={{ maxWidth: "123px" }}
            />
          </>
        )
      },
      urls: {
        //live: "http://steampunkgbg.se",
        git: "https://gitlab.com/narnigrin/steampunk-gotheborg"
      },
      icons: [
        {
          icon: "screen"
        },
        {
          icon: "mobile",
          text: "Responsive & mobile-first"
        },
        {
          icon: "print",
          text: "Contact cards, posters, flyers"
        },
        {
          icon: "branding"
        },
        {
          icon: "code",
          text: `Google calendar integration,\nmember log-in,\ncustomised WP backend`
        },
        {
          icon: "non-profit"
        }
      ],
      bg: {
        image: "steampunkgotheborg-lg-1.jpg"
      },
      screens: {
        desktop: "spgbg-desktop.jpg",
        tablet: "spgbg-tablet.jpg",
        mobile: "spgbg-mobile.png"
      }
    },
    {
      id: "findemil",
      content: {
        title: "#FindEmil",
        text: `<p>What do you do when your child suddenly goes missing and the other parent is the suspect? It&nbsp;turns out you panic for a year, and then ask <i>everyone</i> for&nbsp;help.</p>
          <p>The site uses a percent-based Masonry grid for the desktop layout and a colour scheme based on <a href="http://ethanschoonover.com/solarized">Solarized</a>. There's a custom timeline script and content is parsed from Markdown for simplicity. Flyers and posters were also printed, getting the case some attention in the&nbsp;media.</p>
          <p>This is one of those sites you'd prefer wasn't needed at all, but for now, it&nbsp;<a href="http://findemil.erikanilsson.eu">remains&nbsp;live</a>.</p>`
      },
      urls: {
        live: "http://findemil.erikanilsson.eu"
      },
      icons: [
        {
          icon: "screen"
        },
        {
          icon: "mobile"
        },
        {
          icon: "code",
          text: "Custom jQuery timeline"
        },
        {
          icon: "print",
          text: "Flyers & posters"
        }
      ],
      bg: {
        image: "findemil-lg.jpg"
      },
      screens: {
        desktop: "findemil-desktop.png",
        tablet: "findemil-tablet.jpg",
        mobile: "findemil-mobile.png"
      }
    },
    {
      id: "davidtess",
      content: {
        title: "David & Tess",
        text: `<p>David and Tess got married on 2&nbsp;August 2014. The&nbsp;design of the web site (a wedding gift to the couple) matches the wedding invitations and decorations and helps the wedding guests find their way to and around the big&nbsp;day.</p>
        <p>The photos at the bottom are semi-random&mdash;there's always at least one picture of the couple themselves and one of the engagement&nbsp;rings. The&nbsp;lace border, the two-hearts seal and the colours purple and gold were requested specifically by the&nbsp;couple.</p>`
      },
      urls: {
        archived: "http://erikanilsson.eu/davidtess"
      },
      icons: [
        {
          icon: "screen"
        },
        {
          icon: "mobile"
        },
        {
          icon: "non-profit"
        }
      ],
      bg: {
        image: "davidtess2014-lg.jpg",
        position: ""
      },
      screens: {
        desktop: "davidtess-desktop.jpg",
        tablet: "davidtess-tablet.jpg"
      }
    },
    {
      id: "spkon",
      content: {
        title: "Steampunk convention 2013",
        text: `<p>A re-design of the web site for Sweden's first Steampunk convention, making the site much more appealing and easy to use as well as mobile-friendly. The&nbsp;site retained some graphics from an earlier version, but the layout and most styling and photographic elements&mdash;which were based around public-domain images of pieces of engineering and old blueprints, to match the philosophy and aesthetic of steampunk&mdash;were new.</p>
          <p>I also designed a convention booklet and some other print materials in a similar&nbsp;style.</p>`
      },
      icons: [
        {
          icon: "screen"
        },
        {
          icon: "mobile",
          text: "Mobile-friendly & responsive"
        },
        {
          icon: "print"
        },
        {
          icon: "code",
          text: "Custom 'latest news' script"
        },
        {
          icon: "non-profit"
        }
      ],
      bg: {
        image: "steampunk-konvent-lg.jpg",
        position: "0%"
      },
      screens: {
        desktop: "spkon-desktop.jpg",
        tablet: "spkon-tablet2.jpg",
        mobile: "spkon-mobile.jpg"
      }
    },
    {
      id: "ivfdf",
      content: {
        title: "IVFDF 2012 Aberdeen",
        text: `<p>A complete visual identity&mdash;based on the King's College crown, a symbol for the University of Aberdeen, and the dancing stick-men drawn by a fellow member of the planning committee&mdash;and all printed material, from posters and booklets to umbrellas and wristbands, for&nbsp;the Inter-Varsity Folk Dance Festival when it came to Aberdeen, Scotland, for the first&nbsp;time.</p>
          <p>IVFDF is a weekend-long festival arranged at a different British university each year. A challenge for 2012 was to make the festival look more exciting than ever so as to motivate the southern contingent to travel all the way up north&mdash;so the slogan ended up being "It's closer than you think!" and we took every chance to point out the friendliness (and perhaps silliness) of the planning crew and this particular festival.</p>
          <img src="/images/portfolio/ivfdf-thumb2.jpg" class="inline-photo simple quadruple">
          <img src="/images/portfolio/ivfdf-thumb-tshirts.jpg" class="inline-photo simple quadruple">
          <img src="/images/portfolio/ivfdf-thumb3.jpg" class="inline-photo simple quadruple">
          <img src="/images/portfolio/ivfdf-thumb-brolly.jpg" class="inline-photo simple quadruple">`
      },
      icons: [
        {
          icon: "print"
        },
        {
          icon: "branding"
        },
        {
          icon: "non-profit"
        }
      ],
      bg: {
        image: "ivfdf2012-lg.jpg",
        position: "left"
      }
    },
    {
      id: "paragon",
      content: {
        title: "Paragon Design UF",
        text: `<p>A web development and design outfit run as a Young Enterprise business, Paragon Design UF earned a ‘Best web site of the year’ award from the regional YE office for the clear and attractive way in which our web site communicated the business’s concept and&nbsp;ideas.</p>
          <p>I was the head designer for the project and also developed Paragon Design's brand identity&mdash;the cute upwards-pointing @-sign of the logo symbolising constant improvement, and the "sidebar" of green and red lines tying the brand together.</p>
          <p>The site was fixed-width, because this was 2004 and the term 'responsive design' <a href="https://en.wikipedia.org/wiki/Responsive_web_design#History" target="_blank">wasn't invented</a>.</p>`
      },
      icons: [
        {
          icon: "screen"
        },
        {
          icon: "print"
        },
        {
          icon: "branding"
        },
        {
          icon: "award",
          text: `Young Enterprise \nSkaraborg 'Best \nweb site of the year' \naward`
        }
      ],
      bg: {
        image: "paragon-lg2.jpg",
        position: "20%"
      },
      screens: {
        desktop: "paragon-desktop.jpg",
        tablet: "paragon-tablet.jpg"
      }
    }
  ],
  otherClients: [
    {
      url: "http://www.axiell.se",
      tooltip: "Lead developer on elib.se in 2014",
      label: "Axiell Media"
    },
    {
      url: "http://www.clubcosmos.net",
      tooltip: "Sketched a web site mock-up for a time-travel themed event",
      label: "Club Cosmos"
    },
    {
      url: "http://www.hammarbyfotboll.se/",
      tooltip: "Part of developer team for their new 2016 site",
      label: "Hammarby Fotboll"
    },
    {
      url: "https://www.hellyhansen.com",
      tooltip:
        "Part of the developer team on HH's brand site, led or contributed to multiple seasonal campaigns from 2014 to 2018",
      label: "Helly Hansen"
    },
    {
      url: "http://hgcapital.com",
      tooltip:
        "Lead developer and internal project lead for major site overhaul in 2017",
      label: "HgCapital"
    },
    {
      url: "http://mariabohm.com",
      tooltip: "Re-designed and built her new web presence in 2013",
      label: "Maria Bohm"
    },
    {
      url: "http://patronka.pl/",
      tooltip: "Converted a static site to Drupal",
      label: "Patronka Sp. z o. o"
    },
    {
      url: "https://www.volvoce.com",
      tooltip: "Developer on a campaign site for Volvo Ocean Race",
      label: "Volvo Construction Equipment"
    },
    {
      url: "https://vifast.se",
      tooltip:
        "Developed a WordPress site with some custom API integrations for this Kinnarps AB sister company",
      label: "ViFast AB"
    },
    {
      url: "https://tool.builders",
      label: "Tool.builders",
      tooltip: "Worked on their campaign tool, Phonebank.RED"
    },
    {
      url: "https://bizzcoo.com",
      label: "Bizzcoo",
      tooltip:
        "Developed new amazing features for their skills & planning platform"
    }
  ]
};

const about = {
  markup: () => (
    <>
      <blockquote>
        <p>
          ➤ <b>masala</b> /mə'sɑ:lə/
          <br />
          <b>1.</b> <i>(n.)</i> A varying blend of spices used in Indian
          cooking, most often containing cardamom, coriander, mace, etc.
          <br />
          <b>2.</b> <i>(adj., Hinglish)</i> spicy; dramatic.
        </p>
        <p>
          <b>Etymology:</b> Hindi मसाला <i>(masālā)</i> spice(s), from Persian
          مصلحت <i>(maslahat)</i> affair, policy, best thing to do; derived from
          Arabic صَلَحَ <i>(ṣalaḥa)</i> be fit, competent, usable.
          <br />
          <cite>
            <a href="https://www.merriam-webster.com/dictionary/masala">
              Merriam-Webster
            </a>
            ,{" "}
            <a href="https://www.collinsdictionary.com/dictionary/english/masala">
              Collins
            </a>
            , <a href="https://en.wiktionary.org/wiki/masala">Wiktionary</a>
          </cite>
        </p>
      </blockquote>

      <p>
        <img
          src="images/about7a.jpg"
          alt="The author, looking to the left with a pensive and focused expression. The reflection of a screen is just about visible in his large glasses."
          className="ring-2 ring-offset-2 ring-slate-400"
        />
        {/* TODO on the choice of image */}
      </p>
      <p>
        I'm a designer and a developer. I push the pixels and write the code to
        interfaces like this one. I've also worked with print and illustration,
        and I cheerfully apply a healthy dose of <strong>usability</strong>{" "}
        nerdery to everything I do—even those things to which the term
        “usability” isn't normally applied.
      </p>
      <p>
        You'll find more on all that in my{" "}
        <Link to="/portfolio">portfolio</Link> and{" "}
        <Link to="/skills">skill graphs</Link>.
      </p>
      <p>
        So I'm a pixel pusher-tag mangler-music maker-hair splitter-storysmith,
        and I've been called{" "}
        <i>Miss There's-More-Than-One-Definition-of-That</i> (that'd be the
        hair-splitting, I'd wager). I also draw, dance and educate, I know far
        more about maths than a lot of people consider necessary, I've been
        known to hawk home-made corsets to unsuspecting friends and just now I'm
        trying to build a kitchen.
      </p>
      <p>
        If you'd like to know more,{" "}
        <a href="mailto:ricky@nilsfred.net">drop me a line</a> or browse around.
        <br />
        You can also find more short-form type info in my{" "}
        <Link to="/profile">profile</Link>.
      </p>
    </>
  )
};

const profile = {
  info: [
    {
      title: "Name",
      content: "Ricky Nilsfred, aka Masala"
    },
    /*{
      title: "Pronouns",
      content: "He/him"
    },*/
    {
      title: "Location",
      content: "Stenstorp/Falköping, Sweden (GMT+1/CET)"
    },
    {
      title: "Roles",
      content: `Frontend developer, 
        React developer, 
        PHP developer, 
        System developer, 
        IT consultant, 
        (etc)`
    },
    {
      title: "Experience",
      content: expYears() + " years"
    },
    {
      title: "Contact",
      html: '<a href="mailto:ricky@nilsfred.net">ricky@nilsfred.net</a> • <a href="tel:+46706825164">+46(0)706-825164</a>'
    },
    {
      title: "Degrees",
      html: `BSc (Hons) Mathematics, University of Aberdeen<br>
               PGCert Information Technology, University of Aberdeen<br>
               <span class="opacity-50">an unfinished <i>Part III</i> (~MSc) from Cambridge, of which I am very proud</span>`
    },
    /*{
      title: "Dream job",
      content: `A secure job without "crunches", that lets me play with React and/or 
                other frontend frameworks, practise my PHP or learn more backend 
                languages, and occasionally do some web design. A heterogenous team
                of colleagues who are enthusiastic about their profession and care 
                about each other and other people. A role  where I contribute to 
                improving the world or people's lives, or solving big, important 
                problems.`
    },*/
    {
      title: "Availability",
      html: "<strong>Available for full-time employment</strong>"
    },
    {
      title: "Current status",
      html: `Looking for new professional challenges. Renovating my flat. 
             Trying to learn Arabic on Duolingo (at least I can say "your cat is American" and other useful everyday phrases).`
    }
  ],
  contact: [
    {
      icon: <Linkedin title="LinkedIn" />,
      color: "rgb(0,119,181)",
      alt: "LinkedIn",
      url: "https://linkedin.com/in/nilsfred"
    },
    {
      icon: <Gitlab title="GitLab (personal/hobby projects live here)" />,
      color: "rgb(107,79,187)",
      alt: "GitLab (personal/hobby projects live here)",
      url: "https://gitlab.com/narnigrin"
    },
    {
      icon: <Stackoverflow title="Stack Overflow" />,
      color: "rgb(244,128,36)",
      alt: "Stack Overflow",
      url: "http://stackoverflow.com/users/4042323/masala"
    },
    {
      icon: <Github title="GitHub (not my most active account)" />,
      color: "#333",
      alt: "GitHub (not my most active account)",
      url: "https://github.com/narnigrin"
    },
    {
      icon: <Minutemailer title="E-mail me" />,
      //color: "rgb(143,200,32)",
      alt: "E-mail me",
      url: "mailto:ricky@nilsfred.net"
    },
    {
      icon: <Facebook title="Facebook, if you must" />,
      color: "rgb(59,89,152)",
      alt: "Facebook, if you must",
      url: "https://facebook.com/narnigrin"
    },
    {
      icon: <Duolingo title="Duolingo :)" />,
      logo: "images/duo_round.svg",
      color: "#50C800",
      alt: "Duolingo :)",
      url: "https://duolingo.com/profile/Eri160727"
    }
  ]
};

export { skills, portfolio, about, profile };
