import { useState, useRef, useEffect } from "react";
import "../css/slider.css";

const Slider = ({ slides, navLabel }) => {
  const [current, setCurrent] = useState(1);
  const [maxSlideHeight, setMaxSlideHeight] = useState(null);
  const titleNav = useRef();
  const wrapper = useRef();

  const isCurrent = (i) => i === current;
  const isPrev = (i) => (i + 1) % slides.length === current;
  const isNext = (i) => i === (current + 1) % slides.length;
  const isAfterNext = (i) => i === (current + 2) % slides.length;

  const navigate = (direction) => {
    let slidesCount = slides.length;

    setCurrent((curState) => {
      let modifier = 1;
      if (direction < 0) modifier = -1;

      let newCurrent = curState + modifier;
      // shenanigans below bc % is remainder, not modulo, and we need to avoid negatives
      return ((newCurrent % slidesCount) + slidesCount) % slidesCount;
    });
  };
  const goPrevious = () => navigate(-1);
  const goNext = () => navigate(1);

  const resizeHandler = () => {
    if (!wrapper.current) return;

    let max = 0,
      h,
      children = wrapper.current.children;

    for (let i = 0; i < children.length; i++) {
      children[i].style.height = "auto";
      h = children[i].offsetHeight;
      children[i].style.height = "";

      if (h > max) max = h;
    }

    setMaxSlideHeight(max);
  };

  useEffect(() => {
    resizeHandler();
    window.addEventListener("resize", resizeHandler);

    return () => {
      window.removeEventListener("resize", resizeHandler);
    };
  }, []);

  // Double slides array if fewer than 5. It's a bit naïve but fixes some animation/movement issues
  let slidesFix = slides;
  if (slides.length < 5) slidesFix = [...slides, ...slides];

  // Pick up the headers from the slides - inelegant, but saves us making the api too complicated
  let slideHeaders = slides.map((slide, i) => {
    let firstHeader = slide.props.children.filter(
      (child) => ["h1", "h2", "h3", "h4"].indexOf(child.type) > -1
    );
    if (!!firstHeader) return firstHeader[0];
    else return <h2>Slide {i}</h2>;
  });

  return (
    <section className="slider-3d" style={{ height: maxSlideHeight + "px" }}>
      <nav className="slider-3d__nav" aria-label={navLabel}>
        <button
          className="slider-3d__arrow slider-3d__arrow--prev"
          onClick={goPrevious}
          onTouchStart={goPrevious}
          title="Previous"
        >
          Previous slide
        </button>
        <button
          className="slider-3d__arrow slider-3d__arrow--next"
          onClick={goNext}
          onTouchStart={goNext}
          title="Next"
        >
          Next slide
        </button>

        <ul className="slider-3d__title-nav" ref={titleNav}>
          {slideHeaders.map((sh, i) => (
            <li
              key={`slideHeaders_${i}`}
              className={isCurrent(i) ? "slider-3d__title-nav--current" : ""}
              id={`slideHeader_${i}`}
            >
              <button onClick={() => setCurrent(i)}>{sh}</button>
            </li>
          ))}
        </ul>
      </nav>
      <ul className="slider-3d__slides" ref={wrapper}>
        {slidesFix.map((slide, i) => (
          <Slide
            key={i}
            isCurrent={isCurrent(i)}
            isPrev={isPrev(i)}
            isNext={isNext(i)}
            isAfterNext={isAfterNext(i)}
            goPrevious={goPrevious}
            goNext={goNext}
            titleNavRef={titleNav}
            index={i}
          >
            {slide}
          </Slide>
        ))}
      </ul>
    </section>
  );
};

const Slide = ({
  isCurrent,
  isPrev,
  isNext,
  isAfterNext,
  goPrevious,
  goNext,
  children,
  titleNavRef,
  index
}) => {
  const clickSlide = (ev) => {
    let isVisible =
      !titleNavRef.current ||
      window
        .getComputedStyle(titleNavRef.current)
        .getPropertyValue("display") !== "none";

    // If titleNav is visible or slide is current, click should NOT cause navigation.
    if (isCurrent || isVisible) return;

    ev.stopPropagation();
    if (isPrev) goPrevious();
    if (isNext) goNext();
  };

  return (
    <li
      className={
        "slider-3d__slide" +
        (!!isCurrent ? " slider-3d__slide--current z-[500]" : "") +
        (!!isPrev ? " slider-3d__slide--prev" : "") +
        (!!isNext ? " slider-3d__slide--next" : "") +
        (!!isAfterNext ? " slider-3d__slide--afternext " : "")
      }
      aria-current={!!isCurrent ? "true" : "false"}
      onClick={clickSlide}
      aria-labelledby={`slideHeader_${index}`}
    >
      {children}
    </li>
  );
};

export { Slider, Slide };
