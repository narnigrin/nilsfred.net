import React, { useEffect, useRef } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { portfolio as data } from "../../data";
import { PortfolioItem } from "../PortfolioItem";
import Tooltip from "../Tooltip";

const Portfolio = () => {
  const portfolioUl = useRef(null);
  const params = useParams();
  const navigate = useNavigate();

  const unSetActive = (ev) => {
    if (!!portfolioUl.current && !portfolioUl.current.contains(ev.target)) {
      // 2nd half might not be necessary bc of stopPropagation in PortfolioItem ^
      navigate("/portfolio");
    }
  };

  useEffect(() => {
    document.addEventListener("click", unSetActive);

    return () => {
      document.removeEventListener("click", unSetActive);
    };
  });

  return (
    <main>
      <style
        dangerouslySetInnerHTML={{
          __html: `:root {
            --theme-primary: #0369a1; /* theme(colors.sky.700) */
            --theme-primary-fade: #0369a1dd;
            --theme-dark: #0c4a6e; /* theme(colors.sky.900) */
          }`
        }}
      />

      <h1 className="hide-with-menu">Portfolio</h1>

      <ul className="Portfolio mb-8" ref={portfolioUl}>
        {data.projects.map((item) => (
          <PortfolioItem
            data={item}
            key={item.id}
            isActive={params.project === item.id}
            onClick={() => navigate(item.id)}
            portfolioElt={portfolioUl}
          />
        ))}
      </ul>

      <h2 className="my-2 mx-auto text-center scriptcaps">
        I've also Worked with
      </h2>
      <p className="max-w-none text-center">
        {data.otherClients
          .sort((a, b) => a.label > b.label)
          .map((item, i) => (
            <React.Fragment key={`otherClient_${i}`}>
              {i > 0 && <> • </>}
              <Tooltip
                href={item.url}
                tooltip={item.tooltip}
                className="inline-block"
                triggerElement="a"
              >
                {item.label}
              </Tooltip>
            </React.Fragment>
          ))}
      </p>
    </main>
  );
};

export default Portfolio;
