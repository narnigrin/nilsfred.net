import { useState, useEffect } from "react";
import { Slider } from "../Slider";
import { RadarChart } from "../RadarChart";
import { DonutChart } from "../DonutChart";
import { skills as data } from "../../data";
import Tooltip from "../Tooltip";

const Skills = () => {
  const getNegative = () => {
    const negative = [
      "Lisp",
      "COBOL",
      "Assembly",
      "Fortran",
      "Square dance",
      "Baseball"
    ];
    let rand = Math.floor(Math.random() * negative.length);
    return negative[rand];
  };

  const [activeBar, setActiveBar] = useState(-1);
  const [activeNumber, setActiveNumber] = useState(-1);
  const [negativeText, setNegativeText] = useState(null);

  // Tap outside the barchart bars or numbers clears open tooltips
  const clickOutside = (ev) => {
    if (!ev.target?.closest(".Skills-slide__barchart__bar")) {
      setActiveBar(-1);
    }
    if (!ev.target?.closest(".Skills-numbers__list, .Skills-numbers__child")) {
      setActiveNumber(-1);
    }
  };

  useEffect(() => {
    document.addEventListener("touchstart", clickOutside);
    setNegativeText(getNegative());

    return () => {
      document.removeEventListener("touchstart", clickOutside);
    };
  }, []);

  return (
    <main className="Skills-page">
      <style
        dangerouslySetInnerHTML={{
          __html: `:root {
            --theme-primary: #6b21a8; /* theme(colors.purple.800) */
            --theme-primary-fade: #6b21a8dd;
            --theme-dark: #581c87; /* theme(colors.purple.900) */
          }`
        }}
      />

      <h1 className="hide-with-menu mb-16 mt-0 text-center scriptcaps">
        Skill graph
      </h1>
      <Slider
        slides={[
          <div className="Skills-slide Skills-slide--daily">
            <h2>Recent work</h2>
            <DonutChart data={data.daily} />
          </div>,
          <div className="Skills-slide">
            <h2>Key skills</h2>
            <ul className="Skills-slide__barchart">
              {data.skills.map((item, i) => (
                <BarchartBar
                  key={"barchart_" + i}
                  text={item.text}
                  percentSize={item.size}
                  tooltip={item.tooltip}
                  toggleActive={() => {
                    if (activeBar === i) setActiveBar(-1);
                    else setActiveBar(i);
                  }}
                  isActive={activeBar === i}
                />
              ))}
              <BarchartBar
                text={negativeText}
                percentSize={5}
                className="textshadow-white text-cyan-600 relative -left-[5%] pl-[5%] indent-1.5"
              />
            </ul>
          </div>,
          <div className="Skills-slide">
            <h2>Interests</h2>
            <RadarChart data={data.interests} />
          </div>
        ]}
        navLabel="Skills slider"
      />

      <div className="Skills-numbers">
        <div className="Skills-numbers__list">
          {data.numbers.map((item, i) => (
            <BigNumber
              data={item}
              key={"numbers_" + i}
              isActive={activeNumber === i}
              toggleActive={() => {
                if (activeNumber === i) setActiveNumber(-1);
                else setActiveNumber(i);
              }}
            />
          ))}
        </div>
      </div>
    </main>
  );
};

const BarchartBar = ({
  text,
  percentSize,
  className,
  tooltip,
  toggleActive,
  isActive
}) => {
  if (typeof percentSize === "undefined") percentSize = 50;
  if (typeof className === "undefined") className = "";

  return (
    <li
      className={"Skills-slide__barchart__bar " + className}
      style={{ width: percentSize + "%" }}
      onTouchStart={toggleActive}
    >
      <Tooltip
        triggerElement="div"
        tooltip={tooltip}
        className="h-full w-full text-left"
        lockOpen={isActive}
      >
        {text}
        {!!tooltip && (
          <button className="Skills-slide__barchart__bar--info-button info-button">
            i<span className="sr-only">nfo</span>
          </button>
        )}
      </Tooltip>
    </li>
  );
};

const BigNumber = ({ data, isActive, toggleActive }) => {
  let Markup,
    classes = data.classes || "";

  const Digit = ({ d }) => (
    <span
      className="Skills-numbers__child digit"
      dangerouslySetInnerHTML={{ __html: d }}
    />
  );
  const Text = ({ t, classes }) => (
    <small
      className={"Skills-numbers__child " + (classes || "")}
      dangerouslySetInnerHTML={{ __html: t }}
    />
  );

  switch (data.style) {
    case "number-first":
      Markup = () => (
        <>
          <Digit d={data.content.digit} />
          <Text t={data.content.text} classes="text-left" />
        </>
      );
      break;
    case "number-middle":
      Markup = () => (
        <>
          <Text classes="text-right" t={data.content.text[0]} />
          <Digit d={data.content.digit} />
          <Text t={data.content.text[1]} classes="text-left" />
        </>
      );
      break;
    case "number-last":
    default:
      Markup = () => (
        <>
          <Text t={data.content.text} classes="text-right" />
          <Digit d={data.content.digit} />
        </>
      );
      break;
  }

  return (
    <Tooltip
      tooltip={data.tooltip}
      className={classes + " Skills-numbers__list-item"}
      onTouchStart={toggleActive}
      lockOpen={isActive}
    >
      <Markup />
    </Tooltip>
  );
};

export default Skills;
