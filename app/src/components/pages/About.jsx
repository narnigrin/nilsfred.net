import Icon from "../Icon";
import { about as data } from "../../data";

const About = () => {
  return (
    <main>
      <style
        dangerouslySetInnerHTML={{
          __html: `:root {
            --theme-primary: #991b1b; /* theme(colors.red.800) */
            --theme-primary-fade: #991b1bdd;
            --theme-dark: #7f1d1d; /* theme(colors.red.900) */
          }`
        }}
      />
      <h1>
        <span className="scriptcaps">O</span>n Masala
      </h1>
      <div className="columns-xs gap-10 use-color-em">
        <data.markup />
        <p>
          <a href="/CV.pdf" download="CV_RickyMasalaNilsfred.pdf">
            <Icon type="doc" className="inline-icon" /> Resumé (pdf)
          </a>
        </p>
      </div>
    </main>
  );
};

export default About;
