import { profile as data } from "../../data";
import Tooltip from "../Tooltip";

const Profile = () => (
  <main className="mx-auto max-w-3xl w-full">
    <style
      dangerouslySetInnerHTML={{
        __html: `:root {
          --theme-primary: #16a34a; /* theme(colors.green.600) */
          --theme-primary-fade: #16a34add;
          --theme-dark: #14532d; /* theme(colors.green.900) */
        }`
      }}
    />

    <h1 className="whitespace-nowrap">
      Hi, I'm <span className="scriptcaps">Masala</span>
    </h1>

    <img
      src="images/profile1.jpg"
      className="w-1/5 min-w-[6rem] float-left mr-6 mb-2 mt-2 ring-2 ring-offset-2 ring-slate-400"
      style={{ width: "calc(20% - .75rem)" }}
      alt="Portrait of Masala, looking towards the camera and smiling against a light-coloured background"
    />

    <dl>
      {data.info.map((item, i) => (
        <div
          key={`info_${i}`}
          className="flex flex-wrap sm:flex-nowrap mb-3 use-color-em"
        >
          <dt className="font-bold w-1/5 min-w-[6rem] mr-3 flex-shrink-0 whitespace-nowrap">
            {item.title}:
          </dt>
          <dd>
            {item.content || (
              <div dangerouslySetInnerHTML={{ __html: item.html }} />
            )}
          </dd>
        </div>
      ))}
    </dl>

    <ul className="Contact-card grid gap-4 justify-between mt-16">
      {data.contact.map((item, i) => (
        <li key={`contact_${i}`}>
          <Tooltip
            triggerElement="a"
            href={item.url}
            tooltip={item.alt}
            style={{ backgroundColor: item.color }}
          >
            {!!item.logo ? (
              <img
                src={item.logo}
                alt={item.alt}
                className="absolute inset-0 w-11/12 h-11/12 m-auto"
              />
            ) : (
              item.icon
            )}
          </Tooltip>
        </li>
      ))}
    </ul>
  </main>
);

export default Profile;
