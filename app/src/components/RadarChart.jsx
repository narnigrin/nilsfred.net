import { useState, useEffect } from "react";
import Tooltip from "./Tooltip";

export const RadarChart = ({ data }) => {
  const midPoint = { x: 100, y: 100 }; // Define the diagram midpoint, for clarity
  const numPoints = data.length; // Set a named constant for this

  const [activeIndex, setActiveIndex] = useState(-1);

  const toggleActiveIndex = (index) => {
    setActiveIndex((prev) => {
      if (prev === index) return -1;
      else return index;
    });
  };

  // Tap outside the chart clears open tooltips
  const clickOutside = (ev) => {
    if (!ev.target?.closest(".Radar-chart__spoke, .Radar-chart__labels")) {
      setActiveIndex(-1);
    }
  };

  useEffect(() => {
    document.addEventListener("touchstart", clickOutside);
    return () => {
      document.removeEventListener("touchstart", clickOutside);
    };
  }, []);

  const getAngle = (index) => (index * 2 * Math.PI) / numPoints;

  let dataExtended = data.map((item, i) => ({
    ...item,
    size: typeof item.size !== "undefined" ? item.size : 50,
    angle: getAngle(i)
  }));

  const polarToCartesian = (angle, radius) => {
    if (typeof angle === "undefined")
      throw new Error("Needs angle in radians; angle is undefined");
    if (typeof radius === "undefined") radius = 100;

    return {
      x: radius * Math.sin(angle),
      y: radius * Math.cos(angle)
    };
  };

  /**
   * Helper functions to build up polygons
   */
  const getPoly = (scale, useSize) => {
    if (typeof scale === "undefined") scale = 1;

    let coordPoints = dataExtended.map((item) =>
      polarToCartesian(item.angle, useSize === true ? item.size : 100)
    );

    return coordPoints
      .map(
        (point) =>
          `${scale * point.x + midPoint.x},${scale * point.y + midPoint.y}`
      )
      .join(" ");
  };
  const getBasicPoly = (scale) => getPoly(scale, false);
  const getMainPoly = () => getPoly(1, true);

  // Gets its own function since the syntax of <path> is different from <polygon>
  // And also, it needs to draw itself "backwards" :) :) :)
  const getTextPathPolyDAttr = () => {
    let coordPoints = dataExtended.map((item) =>
      polarToCartesian(-1 * item.angle)
    );
    let pointsString = coordPoints
      .map(
        (point, i) =>
          `${i === 0 ? "M" : "L"} ${point.x + midPoint.x},${
            point.y + midPoint.y
          }`
      )
      .join(" ");
    pointsString += ` L ${coordPoints[0].x + midPoint.x},${
      coordPoints[0].y + midPoint.y
    }`;
    return pointsString;
  };
  /**
   * END polygon helpers
   */

  const diagramScale = 0.83;
  return (
    <>
      <svg
        className="Radar-chart"
        viewBox="0 0 200 200"
        width="200"
        height="200"
        xmlns="http://www.w3.org/2000/svg"
      >
        <defs>
          {data.map((_, i) => (
            <marker
              key={"marker_" + i}
              id={"markerDot_" + i}
              viewBox="0 0 16 16"
              refX="8"
              refY="8"
              markerWidth="8"
              markerHeight="8"
            >
              <circle
                cx="8"
                cy="8"
                r="8"
                className={
                  "markerDot " + (activeIndex === i ? "markerDot--active" : "")
                }
              />
            </marker>
          ))}
        </defs>
        <g
          className="Radar-chart__gridFrames"
          transform={`scale(${diagramScale})`}
          transform-origin="100 100"
        >
          <polygon points={getBasicPoly()} />
          <polygon points={getBasicPoly(0.67)} />
          <polygon points={getBasicPoly(0.33)} />
          {dataExtended.map((item, i) => {
            let coords = polarToCartesian(item.angle);
            return (
              <polyline
                className={
                  "Radar-chart__spoke " +
                  (activeIndex === i ? "Radar-chart__spoke--active" : "")
                }
                key={"line_" + i}
                points={`${midPoint.x},${midPoint.y} ${coords.x + midPoint.x},${
                  coords.y + midPoint.y
                }`}
                markerEnd={`url(#markerDot_${i})`}
                onTouchStart={() => toggleActiveIndex(i)}
                onMouseOver={() => setActiveIndex(i)}
                onMouseOut={() => setActiveIndex(-1)}
              />
            );
          })}
        </g>
        <polygon
          className="Radar-chart__diagram"
          points={getMainPoly()}
          transform={`scale(${diagramScale})`}
          transform-origin="100 100"
          style={{ pointerEvents: "none" }}
        />
        <path
          id="textGuidePoly"
          d={getTextPathPolyDAttr()}
          style={{ stroke: "none", fill: "none" }}
        />
        <g
          className="Radar-chart__labels"
          transform={`rotate(${(-1 * 360) / (2 * numPoints)})`}
          transform-origin="100 100"
        >
          <text>
            {dataExtended.map((item, i) => {
              /*
                A note on the maths:
                The textPath elements are shifted along the polygon path by a percentage.
                To find it, we first calculate how many % one nth is (for n=numPoints).
                We then multiply by the number of "steps" we've gone along the loop,
                i.e. the label's index in the array, but we have to count backwards
                since angles go CCW and our path goes CW.
                Then we add 1/2 nth to place the text along a side instead of around a corner.
                The transform on g.Radar-chart__labels above finally rotates everything back 
                by 1/2 nth (but in degrees) to get the labels back to their respective spokes.
            */
              let nth = 100 / numPoints;
              return (
                <textPath
                  key={"label_" + i}
                  startOffset={((nth * (numPoints - i) + nth / 2) % 100) + "%"}
                  href="#textGuidePoly"
                  textAnchor="middle"
                  className={
                    "Radar-chart__label " +
                    (activeIndex === i ? "Radar-chart__label--active" : "")
                  }
                  data-label-index={i}
                  onTouchStart={() => toggleActiveIndex(i)}
                  onMouseOver={() => setActiveIndex(i)}
                  onMouseOut={() => setActiveIndex(-1)}
                >
                  {item.text}
                </textPath>
              );
            })}
          </text>
        </g>
      </svg>
      <div>
        {dataExtended.map((item, i) => (
          <Tooltip
            triggerElement="div"
            tooltip={
              item.tooltip && (
                <>
                  <strong>{item.text}</strong>
                  <br />
                  {item.tooltip}
                </>
              )
            }
            lockOpen={activeIndex === i}
            key={"radar_tooltip_" + i}
          />
        ))}
      </div>
    </>
  );
};
