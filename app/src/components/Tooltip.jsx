import { useState, useRef } from "react";
import {
  useFloating,
  autoUpdate,
  offset,
  flip,
  shift,
  useHover,
  useFocus,
  useDismiss,
  useRole,
  useInteractions,
  FloatingArrow,
  arrow,
  useClientPoint
} from "@floating-ui/react";

const Tooltip = ({
  tooltip,
  triggerElement,
  tooltipElement,
  lockOpen,
  placement,
  ...props
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const arrowRef = useRef(null);

  const { refs, floatingStyles, context } = useFloating({
    open: isOpen,
    onOpenChange: setIsOpen,
    middleware: [offset(30), flip(), shift(), arrow({ element: arrowRef })],
    whileElementsMounted: autoUpdate,
    placement: placement || "bottom"
  });

  const hover = useHover(context, { move: false, mouseOnly: true });
  const focus = useFocus(context);
  const dismiss = useDismiss(context);
  const role = useRole(context, { role: "tooltip" });
  const clientPoint = useClientPoint(context); // follows the mouse

  const { getReferenceProps, getFloatingProps } = useInteractions([
    hover,
    focus,
    dismiss,
    role,
    clientPoint
  ]);

  let TriggerElement = !!triggerElement ? triggerElement : "button";
  let TooltipElement = !!tooltipElement ? tooltipElement : "span";
  return (
    <>
      <TriggerElement
        ref={refs.setReference}
        {...getReferenceProps()}
        {...props}
      >
        {props.children}
      </TriggerElement>
      {(isOpen || lockOpen) && !!tooltip && (
        <TooltipElement
          className="tooltip"
          ref={refs.setFloating}
          style={floatingStyles}
          {...getFloatingProps()}
        >
          {tooltip}

          <FloatingArrow
            ref={arrowRef}
            context={context}
            width={16}
            height={8}
            fill="var(--theme-primary-fade)"
          />
        </TooltipElement>
      )}
    </>
  );
};

export default Tooltip;
