export const Skeleton = () => (
  <main className="Skeleton-loader">
    <h1 className="Skeleton-loader__box w-3/5 mx-auto">.</h1>
    <div className="Skeleton-loader__box w-10/12 h-8 mx-auto mb-3" />
    <div className="Skeleton-loader__box w-9/12 h-6 mx-auto mb-3" />
    <div className="Skeleton-loader__box w-1/3 h-6 mx-auto mb-3" />
  </main>
);
