import { useState, useEffect } from "react";
import Tooltip from "./Tooltip";

export const DonutChart = ({ data }) => {
  const midPoint = { x: 100, y: 100 };
  const radius = 100;

  const [activeIndex, setActiveIndex] = useState(-1);
  const toggleActiveIndex = (index) => {
    setActiveIndex((prev) => {
      if (prev === index) return -1;
      else return index;
    });
  };

  // Tap outside the chart clears open tooltips
  const clickOutside = (ev) => {
    if (!ev.target?.closest(".Donut-chart__slice, .Donut-chart__labels")) {
      setActiveIndex(-1);
    }
  };

  useEffect(() => {
    document.addEventListener("touchstart", clickOutside);
    return () => {
      document.removeEventListener("touchstart", clickOutside);
    };
  }, []);

  const normaliseFractions = (ar) => {
    let sum = ar.reduce((curSum, curItem) => curSum + curItem.fraction, 0);
    if (sum === 1) return ar;

    return ar.map((item) => ({
      ...item,
      fraction: item.fraction / sum
    }));
  };

  const polarToCartesian = (angle) => ({
    x: radius * Math.sin(angle) + midPoint.x,
    y: radius * Math.cos(angle) + midPoint.y
  });

  const getSliceCoords = (fraction) => {
    let angle = 2 * Math.PI * (fraction - 0.005);
    let arcFlag = "0";
    if (angle > Math.PI) arcFlag = "1";
    let endCoords = polarToCartesian(angle);

    return `M 100 200 A ${radius},${radius} 0 0 ${arcFlag} ${endCoords.x},${endCoords.y}`;
  };

  // Generate a "gradient" of colours between the two given rgb vectors, based on number of slices.
  // Note on the log function: Since slices are sorted from largest to smallest, a linear function
  // is likely to visually "favour" the start colour. A log function grows at a decreasing rate,
  // thus looks more visually "even"
  const getItemColor = (i) => {
    const start = [14, 116, 144],
      end = [192, 132, 252],
      numSlices = data.length;

    let wt = (4 * Math.log(i + 1)) / numSlices;
    const weight = (i) => Math.round(start[i] * (1 - wt) + end[i] * wt);
    return `rgb(${weight(0)}, ${weight(1)}, ${weight(2)})`;
  };

  // Figure out the slice offsets and stick them on the data object
  // Then sort data by fraction size
  let currentAngleOffset = 0;
  data = normaliseFractions(data);
  for (let i = 0; i < data.length; i++) {
    data[i].offset = currentAngleOffset;
    currentAngleOffset += 360 * data[i].fraction;
  }
  data.sort((a, b) => b.fraction - a.fraction);

  return (
    <div className="Donut-chart">
      <svg
        viewBox="0 0 200 200"
        width="200"
        height="200"
        xmlns="http://www.w3.org/2000/svg"
      >
        {data.map((item, i) => {
          return (
            <g
              className={
                "Donut-chart__slice " +
                (activeIndex === i ? "Donut-chart__slice--active" : "")
              }
              style={{ color: getItemColor(i) }}
              key={`slice_${i}`}
            >
              <path
                d={getSliceCoords(item.fraction)}
                transform={`scale(.85) rotate(-${item.offset})`}
                transform-origin="100 100"
                onTouchStart={() => toggleActiveIndex(i)}
                onMouseOver={() => setActiveIndex(i)}
                onMouseOut={() => setActiveIndex(-1)}
              />
            </g>
          );
        })}
      </svg>

      <ul className="Donut-chart__labels">
        {data.map((item, i) => (
          <li
            className={
              "Donut-chart__label  " +
              (activeIndex === i ? "Donut-chart__label--active" : "")
            }
            style={{ color: getItemColor(i) }}
            key={"label_" + i}
          >
            <Tooltip
              tooltip={
                item.tooltip && (
                  <>
                    <strong>{item.text}</strong>
                    <br />
                    {item.tooltip}
                  </>
                )
              }
              onTouchStart={() => toggleActiveIndex(i)}
              onMouseOver={() => setActiveIndex(i)}
              onMouseOut={() => setActiveIndex(-1)}
              lockOpen={activeIndex === i}
              placement={item.tooltipPlacement}
            >
              <span>{item.text}</span>
              {!!item.tooltip && (
                <span className="info-button inline-block ml-1">
                  i<span className="sr-only">nfo</span>
                </span>
              )}
            </Tooltip>
          </li>
        ))}
      </ul>
    </div>
  );
};
