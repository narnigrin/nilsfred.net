import React, { useEffect, useState, useRef } from "react";
import Icon from "./Icon";
import Tooltip from "./Tooltip";

export const PortfolioItem = ({ data, isActive, onClick, portfolioElt }) => {
  const [openHeight, setOpenHeight] = useState(null);
  const thisItemElt = useRef();

  const handleClick = (ev) => {
    if (!isActive) onClick(ev);
  };

  // TODO: Can we use that clever transition-to-height-auto trick from Kevin Powell?
  const resizeHandler = () => {
    if (!portfolioElt.current || !thisItemElt.current) return;

    // Use flex-direction of .Portfolio to see what layout we're in ...
    let flexDirection = window
      .getComputedStyle(portfolioElt.current)
      .getPropertyValue("flex-direction");
    let isVertical =
      flexDirection === "column" || flexDirection === "column-reverse";

    // ... and only do anything if we're in vertical
    if (isVertical) {
      portfolioElt.current.style.display = "block"; // Temporarily un-flex it
      let h = thisItemElt.current.offsetHeight;
      setOpenHeight(h);
      portfolioElt.current.style.display = ""; // Reset the flex
    } else {
      setOpenHeight(null);
    }
  };

  useEffect(() => {
    resizeHandler();
    window.addEventListener("resize", resizeHandler);

    return () => {
      window.removeEventListener("resize", resizeHandler);
    };
  });

  let Content;
  if (!!data.content.Jsx) Content = data.content.Jsx;
  else if (!!data.content.text)
    Content = () => (
      <div
        className="use-color-em"
        dangerouslySetInnerHTML={{ __html: data.content.text }}
      />
    );

  return (
    <li
      className={
        "Portfolio__item " + (!!isActive ? "Portfolio__item--active" : "")
      }
      onClick={handleClick}
      onTouchStart={handleClick}
      style={{ flexBasis: !!openHeight ? `${openHeight}px` : "" }}
      ref={thisItemElt}
    >
      <PortfolioBgs bgObject={data.bg} />

      <div className="Portfolio__item__wrapper">
        <h2 className="Portfolio__item__title">{data.content.title}</h2>
        <div
          className={`Portfolio__item__content ${
            !!data.screens && !!data.screens.desktop && "has-screens"
          }`}
        >
          {!!data.urls && (
            <PortfolioUrls urlsObj={data.urls} itemId={data.id} />
          )}

          {!!data.icons && (
            <PortfolioIcons iconsArray={data.icons} itemId={data.id} />
          )}

          {!!Content && <Content />}
        </div>
      </div>

      {!!data.screens && !!data.screens.desktop && (
        <PortfolioScreens screens={data.screens} />
      )}
    </li>
  );
};

// Un-exported, single-concern "sub-components"
// to make the main PortfolioItem component less of a mess

const PortfolioBgs = ({ bgObject }) => (
  <div
    className="Portfolio__item__bg"
    style={{
      backgroundImage: `url('/images/portfolio/${bgObject.image}')`,
      backgroundPosition: !!bgObject.position
        ? `top ${bgObject.position}`
        : "top center"
    }}
  >
    <div
      className="Portfolio__item__bg--small"
      style={{
        backgroundImage: `url('/images/portfolio/SM-${bgObject.image}')`,
        backgroundPosition: !!bgObject.position
          ? `top ${bgObject.position}`
          : "top center"
      }}
    />
  </div>
);

const PortfolioUrls = ({ urlsObj, itemId }) => {
  let [urlsArray, setUrlsArray] = useState([]);

  useEffect(() => {
    let ar = Object.keys(urlsObj).map((key) => ({
      type: key,
      url: urlsObj[key],
      text: urlsObj[key].replace(/http(s?):\/\//, "")
    }));
    setUrlsArray(ar);
  }, [urlsObj]);

  return (
    <aside className="Portfolio__item__urls">
      {urlsArray.map((item) => (
        <p className="m-0" key={itemId + "_urls_" + item.type}>
          {item.type === "archived" && "Archived: "}
          <a href={item.url}>{item.text}</a>
        </p>
      ))}
    </aside>
  );
};

const PortfolioIcons = ({ iconsArray, itemId }) => {
  const map = {
    screen: {
      typeKey: "desktop",
      defaultLabel: "Web design"
    },
    mobile: {
      typeKey: "mobile",
      defaultLabel: "Responsive"
    },
    print: {
      typeKey: "book",
      defaultLabel: "Print"
    },
    "non-profit": {
      typeKey: "strike-dollar",
      defaultLabel: "Not for profit"
    },
    branding: {
      typeKey: "branding",
      defaultLabel: "Branding"
    },
    code: {
      typeKey: "code",
      defaultLabel: "Code"
    },
    award: {
      typeKey: "award",
      defaultLabel: "Award"
    }
  };

  const [activeIndex, setActiveIndex] = useState(-1);

  // Tap outside the icons clears open tooltips
  const clickOutside = (ev) => {
    if (!ev.target?.closest(".Portfolio__item__icons__icon")) {
      setActiveIndex(-1);
    }
  };

  useEffect(() => {
    document.addEventListener("touchstart", clickOutside);

    return () => {
      document.removeEventListener("touchstart", clickOutside);
    };
  }, []);

  return (
    <aside className="Portfolio__item__icons">
      {iconsArray.map((icon, i) => (
        <Tooltip
          tooltip={icon.text || map[icon.icon].defaultLabel}
          className="inline-block Portfolio__item__icons__icon"
          key={`${itemId}_icon_${i}`}
          onTouchStart={() => {
            if (activeIndex === i) setActiveIndex(-1);
            else setActiveIndex(i);
          }}
          lockOpen={activeIndex === i}
        >
          <Icon type={map[icon.icon].typeKey} />
        </Tooltip>
      ))}
    </aside>
  );
};

const PortfolioScreens = ({ screens }) => {
  let typeClass = "";
  if (!!screens.desktop && !!screens.tablet && !!screens.mobile)
    typeClass = "responsive";

  return (
    <aside className={"Portfolio__item__screens " + typeClass}>
      <img
        src={`/images/portfolio/screens/${screens.desktop || screens.tablet}`}
        className="Portfolio__item__screen Portfolio__item__screen--desktop"
        alt="desktop"
      />
      <img
        src={`/images/portfolio/screens/${
          screens.tablet || screens.mobile || screens.desktop
        }`}
        className="Portfolio__item__screen Portfolio__item__screen--tablet"
        alt="tablet"
      />
      {!!screens.mobile && (
        <img
          src={`/images/portfolio/screens/${screens.mobile}`}
          className="Portfolio__item__screen Portfolio__item__screen--mobile"
          alt="mobile"
        />
      )}
    </aside>
  );
};
