/** @type {import('tailwindcss').Config} */

const colors = require("tailwindcss/colors");

module.exports = {
  content: ["./src/**/*.{html,js,jsx}"],
  theme: {
    extend: {},
    colors: {
      transparent: "transparent",
      current: "currentColor",
      black: colors.black,
      white: colors.white,
      slate: colors.slate,
      cyan: colors.cyan,
      red: colors.red,
      green: colors.green,
      purple: colors.purple,
      teal: colors.teal, // just links??
      sky: colors.sky
    },
    fontFamily: {
      sans: ["Atkinson Hyperlegible", "sans-serif"],
      serif: ["Merriweather", "serif"]
    }
  },
  plugins: [],
  experimental: {
    optimizeUniversalDefaults: true
  },
  safelist: [
    // Active colours for the main menu, as those classes are generated
    "text-green-900",
    "text-red-900",
    "text-purple-900",
    "text-sky-900",
    // Other odds and ends
    "opacity-50",
    "z-[500]"
  ]
};
